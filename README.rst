###############
USB HID Display
###############

.. image:: doc/hardware/usb-hid-display.jpg

Overview
========

This project is a simple USB interface to connect a character display with 
a PC. In addition, the device is also equipped with an IR receiver to allow
recording IR sequences.

The USB interface comprimises of:

- USB HID compatible interface to character display using Get/Set Feature 
  report
- CDC(ACM) interface compatible with `GIRS`_ protocol for IR reception

.. _GIRS: http://www.harctoolbox.org/Girs.html

Status
======

Hardware tested successfully and basic firmware done. Open points:

- Test HID conformance. Current implementation was made based on what I understood
  from reading the USB HID and HUT documents. Function tested with
  self written lcdproc hidraw driver
- Cleanup & optimize code: For sake of simplicity I decided to use ``--stack-auto``
  with sdcc. This however increases code size a lot and my degrade speed.
- IR tested with lirc and girs driver

Hints
=====

Lirc
----

Use the ``girs`` driver with `lirc`_. The device will always report a gap length of 
32768µs after the last pulse. You might need to adjust your remote conf accordingly::

  begin remote

    ...
    gap            30000
    repeat_gap     30000

.. _lirc: https://www.lirc.org

Licensing
=========

The firmware in subdirectory ``firmware`` is licensed under 
`GNU General Public License version 3`_. The use of all other files if not otherwise noted
is subject to `CC BY-SA`_ licensing

.. _CC BY-SA: https://creativecommons.org/licenses/by-sa
.. _GNU General Public License version 3: https://www.gnu.org/licenses/gpl-3.0.html

Copyright
=========

Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>.

References
==========

.. target-notes::
