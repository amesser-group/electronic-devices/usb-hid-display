/**
 *  Copyright (c) 2021-2022 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BASTELINO_PROGRAMMER_H_
#define BASTELINO_PROGRAMMER_H_

#include <compiler.h>
#include <stdint.h>
#include <string.h>

#include "ch554.h"
#include "ch554_usb.h"

#include "display.h"

#define USB_DESCR_WORD(x) (uint8_t)(x & 0xFF), (uint8_t)((x >> 8) & 0xFF)
#define USB_DWORD(x)      (uint8_t)(x & 0xFF), (uint8_t)((x >> 8) & 0xFF), (uint8_t)((x >> 16) & 0xFF), (uint8_t)((x >> 24) & 0xFF)

struct UsbTransferState
{
  union {
    __code  uint8_t *const_buffer;
    __xdata uint8_t *setup_out_buffer;
  };

  int_least16_t      len;
};

struct UsbControlTransaction
{
  struct UsbTransferState transfer_state;

  uint_least8_t           request;

  uint_least8_t           request_typ;

  uint_least8_t           request_recip;

  uint_least16_t          value;

  uint_least16_t          index;
};

/* usb endpoint buffers for CDC, we using single buffer mode */
__xdata __at (0x0000) uint8_t Usb_Ep3_Buffer[MAX_PACKET_SIZE * 2];

/* aliases for application */
__xdata __at (0x0000) uint8_t Usb_CDC_OutBuffer[MAX_PACKET_SIZE];
__xdata __at (0x0040) uint8_t Usb_CDC_InBuffer [MAX_PACKET_SIZE];

/* usb endpoint buffers for HID, also using single buffer mode */
__xdata __at (0x0080) uint8_t Usb_Ep2_Buffer[MAX_PACKET_SIZE*2];

__xdata __at (0x0100) uint8_t Usb_Ep0_Buffer[DEFAULT_ENDP0_SIZE];
__xdata __at (0x0108) uint8_t Usb_Ep1_Buffer[DEFAULT_ENDP1_SIZE];

__idata extern uint8_t usb_config_no;

__idata extern volatile uint8_t System_ticks;
__idata extern uint16_t         System_timer_reload_1ms_div12;

__idata extern uint8_t UsbCdc_ep_out_datalen[2];

/* we must declare all isr routines such that they are visible in main.c */
void DeviceInterrupt(void) __interrupt (INT_NO_USB);
//void Uart0Isr(void) __interrupt (INT_NO_UART0);
void Timer0Isr(void) __interrupt (INT_NO_TMR0);
void Timer2Isr(void) __interrupt (INT_NO_TMR2);
//void JTAG_TimerIsr(void) __interrupt (INT_NO_TMR1);
//void System_PWMIsr(void) __interrupt (INT_NO_PWMX);

void	  System_UpdateClock();
void    System_DelayMillis(uint8_t millis);
uint8_t System_GetSysTicks();


void Usb_Init();

int_fast16_t Usb_GetDescriptor_Setup(__idata struct UsbControlTransaction* transaction);

void UsbCdc_Init();
void UsbCdc_Poll();

void usb_cdc_out_transfer();
void usb_cdc_in_transfer();

int_fast16_t UsbCdc_HandleControlSetup(__idata struct UsbControlTransaction *transaction);
int_fast16_t UsbCdc_HandleControlOut(__idata struct UsbControlTransaction *transaction);
int_fast16_t UsbCdc_ClearFeatureEndpointHalt(uint8_t ep);


void UsbCdc_SysTickIsr();

void UsbCdc_SendMsg(__code const char* msg, uint16_t val);
void UsbCdc_SendHex(__code const char* msg, __xdata const uint8_t *data, uint8_t len);

int_fast16_t UsbHid_HandleControlSetup(__idata struct UsbControlTransaction *transaction);
int_fast16_t UsbHid_HandleControlOut(__idata struct UsbControlTransaction *transaction);


#endif