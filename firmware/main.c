/**
 *  Copyright (c) 2021-2024 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "io.h"

#include <stdio.h>
#include <string.h>

#include "ch554_usb.h"

main()
{
  System_UpdateClock();

  /* timer 0/1 16 bit mode, */
  TMOD   = bT0_M0 | bT1_M0;

#if IRTOY_MODE
  /* timer0/1 clock with fsys/12
   * timer2 clock with fsys, enable capture on both edges */
  T2MOD  = bTMR_CLK | bT2_CLK | bT2_CAP_M0;
#else
  /* timer0/1 clock with fsys/12
   * timer2 clock with fsys/12, enable capture on both edges */
  T2MOD  = bTMR_CLK | bT2_CAP_M0;
#endif

  TR0 = 1;

  /* enable interrupts for all timers */
  ET0 = 1;
  ET2 = 1;

  /* Configure wakeup sources */
  SAFE_MOD  = 0x55;
  SAFE_MOD  = 0xAA;
  WAKE_CTRL = bWAK_BY_USB;

  Usb_Init();

  EA = 1;

  Display_Init();
  UsbCdc_Init();

  /* connect to USB */
  USB_CTRL |= bUC_DEV_PU_EN;

  while(1)
  {
    UsbCdc_Poll();
    Display_Refresh();
  }
}
