/**
 *  Copyright (c) 2021-2024 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdbool.h>

#include "common.h"
#include "io.h"

#define USBCDC_RXFIFO_SIZE (64)

enum
{
  USB_CDC_SET_LINE_CODING        = 0x20,           // Configures DTE rate, stop-bits, parity, and number-of-character
  USB_CDC_GET_LINE_CODING        = 0x21,           // This request allows the host to find out the currently configured line coding.
  USB_CDC_SET_CONTROL_LINE_STATE = 0x22,           // This request generates RS-232/V.24 style control signals.
};

__bit   static          UsbCdc_dtr_;

__xdata uint8_t UsbCdc_linecoding[7];

__idata static uint8_t UsbCdc_rxlen_    = 0;
__idata static uint8_t UsbCdc_rxoffset_ = 0;

__xdata static uint8_t  Girs_rxbuf_[USBCDC_RXFIFO_SIZE+1];
__idata static uint8_t  Girs_rxbuf_fill_ = 0;

#define IRTOY_SAMPLEFIFO_DEPTH ((uint8_t)64)

__xdata static uint16_t UsbCdc_ir_fifo_[IRTOY_SAMPLEFIFO_DEPTH];
__idata static uint8_t  UsbCdc_ir_fifo_in_  = 0;
__idata static uint8_t  UsbCdc_ir_fifo_out_ = 0;

#pragma save
#pragma nooverlay


/** Highbyte or timestamp counter. This counters runs with 12MHz / 256 
 *  and is used as timetick as well
 */
__idata static uint8_t  irtoy_ts_highbyte     = 0;
__idata static uint8_t  irtoy_idle_cnt        = 0;

__idata static uint8_t  irtoy_tc_cnt_ovw      = 0;
__idata static uint8_t  irtoy_tc_last_ir_edge = 0;


__bit   static          irtoy_idle;
__bit   static          irtoy_samplemode;


static uint8_t
PutToIrFifo(uint8_t delta)
{
  uint8_t fifo_level = UsbCdc_ir_fifo_in_ - UsbCdc_ir_fifo_out_;

  if(fifo_level >= IRTOY_SAMPLEFIFO_DEPTH)
    return false;

  UsbCdc_ir_fifo_[UsbCdc_ir_fifo_in_ % IRTOY_SAMPLEFIFO_DEPTH] = (irtoy_tc_cnt_ovw << 8) | delta;
  UsbCdc_ir_fifo_in_++;      

  irtoy_tc_cnt_ovw = 0;

  return true;
}

void Timer2Isr(void) __interrupt (INT_NO_TMR2)
{
  if (TF2)
  {
    if(irtoy_tc_cnt_ovw < 255)
    {
      irtoy_tc_cnt_ovw++;
    }
    else if(PutToIrFifo(255))
    {
      irtoy_idle = 1;
    }

    TF2 = 0;
  }
  else if(EXF2)
  {
    uint8_t ts = RCAP2H;

    if(irtoy_idle)
    {
      irtoy_tc_cnt_ovw = 0;
    }
    else
    {
      if (ts >= irtoy_tc_last_ir_edge)
      { 
        PutToIrFifo(ts - irtoy_tc_last_ir_edge);
      }
      else
      { /* there was an overflow in the mean-time, we need to correct for it */
        irtoy_tc_cnt_ovw--;
        PutToIrFifo(256 + ts - irtoy_tc_last_ir_edge);
      }
    }

    irtoy_tc_last_ir_edge = ts;
    irtoy_idle = 0;
    EXF2 = 0;
  }
}

void
usb_cdc_out_transfer()
{
  UsbCdc_rxlen_     = USB_RX_LEN;
  UsbCdc_rxoffset_  = 0;

  UEP3_CTRL |= UEP_R_RES_NAK;
}

void
usb_cdc_in_transfer()
{
  /* clear all bits below 64. This is part of the statemachine to control short packet sending */
  UEP3_T_LEN = UEP3_T_LEN & 0xC0;
  UEP3_CTRL  |= UEP_T_RES_NAK;
}

static
void UsbCdc_SetLineCoding(__xdata const uint8_t *linecoding)
{
  memcpy(UsbCdc_linecoding, linecoding, sizeof(UsbCdc_linecoding));
}

static
void UsbCdc_SetControlLineState(const __xdata USB_SETUP_REQ* setup_req)
{
  __bit dtr = (setup_req->wValueL & 0x01) != 0;

  if ( !UsbCdc_dtr_ && dtr)
  {
    UEP3_CTRL |= UEP_R_RES_NAK | UEP_T_RES_NAK;

    UEP3_T_LEN    = 0;
    UsbCdc_rxlen_ = 0;

    irtoy_samplemode = false;
  }

  UsbCdc_dtr_ = dtr;

  #if 0
  bool dtr_asserted = setup_req->wValueL & 0x01;
  bool rts_asserted = setup_req->wValueL & 0x02;
#endif
}

int_fast16_t
UsbCdc_HandleControlSetup(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->request )
  {
  case USB_CDC_GET_LINE_CODING:
      memcpy(transaction->transfer_state.setup_out_buffer, UsbCdc_linecoding, sizeof(UsbCdc_linecoding));
      return sizeof(UsbCdc_linecoding);
  case USB_CDC_SET_LINE_CODING:
      return 0;
      break;
  case USB_CDC_SET_CONTROL_LINE_STATE:
      UsbCdc_SetControlLineState((const __xdata USB_SETUP_REQ*)(transaction->transfer_state.setup_out_buffer));
      return 0;
      break;
  default:
      return -1;
      break;
  }
}

int_fast16_t
UsbCdc_HandleControlOut(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->request )
  {
  case USB_CDC_SET_LINE_CODING:
    if(transaction->transfer_state.len != 7)
      return -1;

    UsbCdc_SetLineCoding(transaction->transfer_state.setup_out_buffer);
    return 0;
  default:
    return -1;
  }
}

int_fast16_t
UsbCdc_ClearFeatureEndpointHalt(uint8_t ep)
{
  /* To have this a Non-OP when EP not halted we will
   * clear the TOUT bit instead of assigning the entire bitmask:
   * Transitions STALL -> NAK, NAK -> NAK, ACK -> ACK */

  if(ep & 0x80)
  { /* IN (device->host, transmission*/
    UEP3_CTRL = UEP3_CTRL & (~bUEP_T_TOG) & (~UEP_T_RES_TOUT);

    irtoy_samplemode = false;
  }
  else
  { /* OUT (host->device, reception */
    UEP3_CTRL = UEP3_CTRL & (~bUEP_R_TOG) & (~UEP_R_RES_TOUT);

    UsbCdc_rxlen_ = 0;
    irtoy_samplemode = false;
  }

  return 0;
}

#pragma restore

static uint8_t
IRToy_RefillRxBuf()
{
  uint8_t rxlen = UsbCdc_rxlen_;

  if(UsbCdc_rxoffset_ < rxlen)
  {
    uint8_t len_copy = rxlen - UsbCdc_rxoffset_;

    if( (Girs_rxbuf_fill_ + len_copy) > sizeof(Girs_rxbuf_))
      len_copy = sizeof(Girs_rxbuf_) - Girs_rxbuf_fill_;

    if(len_copy > 0)
    {
      memcpy(Girs_rxbuf_ + Girs_rxbuf_fill_, 
            Usb_CDC_OutBuffer + UsbCdc_rxoffset_, 
            len_copy);

      Girs_rxbuf_fill_ += len_copy;
      UsbCdc_rxoffset_ += len_copy;
    }
  }
  else
  { /* re-enable reception if all bytes taken from fifo */
    EA = 0; 
    if((UEP3_CTRL & MASK_UEP_R_RES) == UEP_R_RES_NAK )
    {
      UEP3_CTRL &= ~UEP_R_RES_NAK; 
    }
    EA = 1;
  }

  return Girs_rxbuf_fill_;
}

static void
IRToy_DropRxBytes(uint8_t cnt)
{
  if (cnt < Girs_rxbuf_fill_)
  {
    memmove(Girs_rxbuf_, Girs_rxbuf_ + cnt, Girs_rxbuf_fill_ - cnt);
    Girs_rxbuf_fill_ -= cnt;
  }
  else
  {
    Girs_rxbuf_fill_ = 0;
  }
}


/** Response to sample mode command */
__code static const char kIRToy_String_SAMP[]     = "S01";

/** Version string IRToy would report, minimum version supported by lirc */
__code static const char kIRToy_String_Version[]  = "V120";

void
UsbCdc_TransmittBuffer()
{
  EA = 0; 
  if((UEP3_CTRL & MASK_UEP_T_RES) == UEP_T_RES_NAK )
  {
    UEP3_CTRL &= ~UEP_T_RES_NAK; 
  }
  else
  {
    UEP3_T_LEN  = 0;
  }
  EA = 1;
}

static void 
UsbCdc_SendIRToyResponse(__code const char* rsp)
{
  uint8_t offset = (UEP3_T_LEN % 64);

  //Display_DebugXData("GIRS cmd ok", Girs_rxbuf_, 8);

  while(offset < sizeof(Usb_CDC_InBuffer))
  {
    uint8_t c = *(rsp++);

    if(c)
      Usb_CDC_InBuffer[offset++] = c;
    else
      break;    
  }

  UEP3_T_LEN = offset;
  UsbCdc_TransmittBuffer();
}

__bit static UsbCdc_ir_data_sent_ = 0;

static void
UsbCdc_HandleIRToy_Sample()
{
  if(irtoy_samplemode)
  {
    if((UEP3_CTRL & MASK_UEP_T_RES) == UEP_T_RES_NAK)
    {
      uint8_t offset = (UEP3_T_LEN % 64);

      if(UsbCdc_ir_fifo_in_ != UsbCdc_ir_fifo_out_)
      {
        if(offset <= (sizeof(Usb_CDC_InBuffer) - 2))
        {
          uint16_t entry = UsbCdc_ir_fifo_[UsbCdc_ir_fifo_out_ % IRTOY_SAMPLEFIFO_DEPTH];
          UsbCdc_ir_fifo_out_++;

          Usb_CDC_InBuffer[offset++] = (entry >> 8) & 0xFF;
          Usb_CDC_InBuffer[offset++] = (entry)      & 0xFF;

          UEP3_T_LEN = offset;
        }

        if(offset > (sizeof(Usb_CDC_InBuffer) - 2))
          UsbCdc_TransmittBuffer();
      }
      else if (irtoy_tc_cnt_ovw > 5)
      { /* did not receive pulse/edge for some time now */
        if(offset > 0)
        {
          UsbCdc_TransmittBuffer();
        }
        else if (UEP3_T_LEN >= 64)
        { /* send a short packet when last packet was > 64 byte */
          UEP3_T_LEN = 0;
          UsbCdc_TransmittBuffer();
        }
      }
    }
  }
  else
  { /* sample mode not active, clear fifo */
    UsbCdc_ir_fifo_out_ = UsbCdc_ir_fifo_in_;
  }
}

void
UsbCdc_Init()
{
  UsbCdc_rxlen_    = 0;
  UsbCdc_rxoffset_ = 0;

  Girs_rxbuf_fill_ = 0;

  EXEN2  = 1;
  CP_RL2 = 1;
  TR2    = 1;
}

#define IRS_RESET 		0x00

void
UsbCdc_Poll()
{
  if(IRToy_RefillRxBuf() >= 1)
  {
    Display_DebugXData("IRToy", Girs_rxbuf_, 1);

    if(Girs_rxbuf_[0] == 's')
    {
      UsbCdc_SendIRToyResponse(kIRToy_String_SAMP);
      irtoy_samplemode = 1;
    }
    else if(Girs_rxbuf_[0] == 'v')
    {
      UsbCdc_SendIRToyResponse(kIRToy_String_Version);
    }
    else if(Girs_rxbuf_[0] == IRS_RESET)
    {
      irtoy_samplemode = 0;
    }

    IRToy_DropRxBytes(1);
  }

  UsbCdc_HandleIRToy_Sample();
}
