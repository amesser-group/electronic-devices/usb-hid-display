/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef USB_DESCRIPTOR_HID_H_
#define USB_DESCRIPTOR_HID_H_

#define USB_HID_REPORT_ID_DISPLAY_ATTRIBUTES_REPORT 1
#define USB_HID_REPORT_ID_CURSOR_POSITION_REPORT    2
#define USB_HID_REPORT_ID_CHARACTER_REPORT          3

#define USB_REPORT_DESCRIPTOR_BYTES \
    0x05, 0x14,                    /* USAGE_PAGE (Alphnumeric Display) */\
    0x09, 0x01,                    /* USAGE (Alphanumeric Display) */\
    0x15, 0x00,                    /* LOGICAL_MINIMUM (0) */\
    0xa1, 0x02,                    /* COLLECTION (Logical) */\
    0x09, 0x20,                    /*   USAGE (Display Attributes Report) */\
    0xa1, 0x02,                    /*   COLLECTION (Logical) */\
    0x09, 0x35,                    /*     USAGE (Rows) */\
    0x09, 0x36,                    /*     USAGE (Columns) */\
    0x09, 0x3d,                    /*     USAGE (Character Width) */\
    0x09, 0x3e,                    /*     USAGE (Character Height) */\
    0x85, 0x01,                    /*     REPORT_ID (1) */\
    0x25, 0x28,                    /*     LOGICAL_MAXIMUM (40) */\
    0x75, 0x06,                    /*     REPORT_SIZE (6) */\
    0x95, 0x04,                    /*     REPORT_COUNT (4) */\
    0xb1, 0x03,                    /*     FEATURE (Cnst,Var,Abs) */\
    0xc0,                          /*   END_COLLECTION */\
    0x75, 0x08,                    /*   REPORT_SIZE (8) */\
    0x95, 0x01,                    /*   REPORT_COUNT (1) */\
    0x25, 0x02,                    /*   LOGICAL_MAXIMUM (2) */\
    0x09, 0x2d,                    /*   USAGE (Display Status) */\
    0xa1, 0x02,                    /*   COLLECTION (Logical) */\
    0x09, 0x2e,                    /*     USAGE (Stat Not Ready) */\
    0x09, 0x2f,                    /*     USAGE (Stat Ready) */\
    0x09, 0x30,                    /*     USAGE (Err Not a loadable character) */\
    0x81, 0x40,                    /*     INPUT (Data,Ary,Abs,Null) */\
    0xc0,                          /*   END_COLLECTION */\
    0x09, 0x32,                    /*   USAGE (Cursor Position Report) */\
    0xa1, 0x02,                    /*   COLLECTION (Logical) */\
    0x85, 0x02,                    /*     REPORT_ID (2) */\
    0x75, 0x08,                    /*     REPORT_SIZE (8) */\
    0x95, 0x01,                    /*     REPORT_COUNT (1) */\
    0x25, 0x27,                    /*     LOGICAL_MAXIMUM (39) */\
    0x09, 0x34,                    /*     USAGE (Column) */\
    0xb1, 0x22,                    /*     FEATURE (Data,Var,Abs,NPrf) */\
    0x25, 0x07,                    /*     LOGICAL_MAXIMUM (7) */\
    0x09, 0x33,                    /*     USAGE (Row) */\
    0xb1, 0x22,                    /*     FEATURE (Data,Var,Abs,NPrf) */\
    0xc0,                          /*   END_COLLECTION */\
    0x09, 0x2b,                    /*   USAGE (Character Report) */\
    0xa1, 0x02,                    /*   COLLECTION (Logical) */\
    0x85, 0x03,                    /*     REPORT_ID (3) */\
    0x75, 0x08,                    /*     REPORT_SIZE (8) */\
    0x95, 0x04,                    /*     REPORT_COUNT (4) */\
    0x26, 0xff, 0x00,              /*     LOGICAL_MAXIMUM (255) */\
    0x09, 0x2c,                    /*     USAGE (Display Data) */\
    0xb2, 0x02, 0x01,              /*     FEATURE (Data,Var,Abs,Buf) */\
    0xc0,                          /*   END_COLLECTION */\
    0x85, 0x04,                    /*   REPORT_ID (4) */\
    0x09, 0x3b,                    /*   USAGE (Font Report) */\
    0xa1, 0x02,                    /*   COLLECTION (Logical) */\
    0x15, 0x00,                    /*     LOGICAL_MINIMUM (0) */\
    0x26, 0xff, 0x00,              /*     LOGICAL_MAXIMUM (255) */\
    0x75, 0x08,                    /*     REPORT_SIZE (8) */\
    0x95, 0x01,                    /*     REPORT_COUNT (1) */\
    0x09, 0x2c,                    /*     USAGE (Display Data) */\
    0x91, 0x02,                    /*     OUTPUT (Data,Var,Abs) */\
    0x95, 0x05,                    /*     REPORT_COUNT (5) */\
    0x09, 0x3c,                    /*     USAGE (Font Data) */\
    0x92, 0x02, 0x01,              /*     OUTPUT (Data,Var,Abs,Buf) */\
    0xc0,                          /*   END_COLLECTION */\
    0xc0                           /* END_COLLECTION */


#endif
