########################
USB HID display firmware
########################

Peripheral assignment
=====================

* Timer 0: System Tick
* Timer 2: IR signal capture

Copyright & License
===================

The USB HID display firmware is distributed under the terms of 
the GNU General Public License version 3 with

Copyright (C) 2021 Andreas Messer <andi@bastelmap.de>

