/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "usb_descriptor_hid.h"

#define HID_INTERFACE_CLASS          0x03
#define HID_INTERFACE_SUBCLASS_NONE  0x00
#define HID_INTERFACE_PROTOCOL_NONE  0x00

#define CDC_COMMUNICATIONS_INTERFACE_CLASS    0x02
#define CDC_COMMUNICATIONS_SUBCLASS_ACM       0x02

#define CDC_COMMUNICATIONS_PROTOCOL_V250      0x01
#define CDC_DATA_INTERFACE_CLASS              0x0A

#define USB_DESCR_TYP_IAD       0x0B

#define USB_COUNTRYCODE_UNSUPPORTED 0x00

/* The following USB Vendor/Pid is taken from OpenOCD source
*  src/jtag/drivers/ulink.c
 *
 * WARNING: Do not sell or redistribute any device using this firmware
 *          The VID is owned by KEIL.
 *
 * We currently re-use the vid in order to avoid adapting the
 * openocd ulink driver. TODO: Use own VID/PID combination */


#if IRTOY_MODE
#define USB_VID 0x04D8
#define USB_PID 0xFD08
#else
#define USB_VID 0xC251
#define USB_PID 0x2710
#endif

#define USB_STRING_DSC_MANUFACTURER 1
#define USB_STRING_DSC_PRODUCT      2
#define USB_STRING_DSC_SERIAL       3

__code uint8_t DevDesc[] =
{
    0x12,USB_DESCR_TYP_DEVICE,
    0x10,0x01, /* USB1.1*/
    0x00,0x00,0x00,
    DEFAULT_ENDP0_SIZE,
    USB_DESCR_WORD(USB_VID), /* vendor id */
    USB_DESCR_WORD(USB_PID), /* product id */

    0x00,0x02, /* device release */
    USB_STRING_DSC_MANUFACTURER,
    USB_STRING_DSC_PRODUCT,
    USB_STRING_DSC_SERIAL,

    0x01, /* number of configurations */
};

__code uint8_t UsbDesc_Report[] = {
  USB_REPORT_DESCRIPTOR_BYTES
};

__code uint8_t CfgDesc[] ={
    0x09, USB_DESCR_TYP_CONFIG,
    USB_DESCR_WORD(107), /* total descriptor length */
    0x03,    /* number of interfaces */
    0x01,    /* configuration value */
    0x00,    /* configuration string index */
    0x80,    /* attributes */
    10 / 2, /* max power */

    /*******************************************************************
     * HID Interface for Display
     *******************************************************************/

    0x09,USB_DESCR_TYP_INTERF,
    0x00, /* interface number */
    0x00, /* alternate setting */
    0x02, /* number of endpoints */
    HID_INTERFACE_CLASS, HID_INTERFACE_SUBCLASS_NONE, HID_INTERFACE_PROTOCOL_NONE,
    USB_STRING_DSC_PRODUCT, /* interface string index */

    /* HID descriptor */
    0x09, USB_DESCR_TYP_HID,
    11,1, /* bcdHID */
    USB_COUNTRYCODE_UNSUPPORTED,
    1, /* number of descriptors */
    USB_DESCR_TYP_REPORT,
    USB_DESCR_WORD(sizeof(UsbDesc_Report)),

    /* mandatory in endpoint */
    0x07,USB_DESCR_TYP_ENDP,
    0x82, /* endpoint address 0x2, direction in */
    0x03, /* interrupt */
    USB_DESCR_WORD(8),   /* max packet size */
    255, /* polling interval */

    /* optional out endpoint */
    0x07,USB_DESCR_TYP_ENDP,
    0x02, /* endpoint address 0x2, direction out */
    0x03, /* interrupt */
    USB_DESCR_WORD(8),   /* max packet size */
    255, /* polling interval */

    /*******************************************************************
     * CDC
     *******************************************************************/

    /* interface association for cdc */
    0x08,USB_DESCR_TYP_IAD,
    0x01, /* first interface number */
    0x02, /* interface count */
    CDC_COMMUNICATIONS_INTERFACE_CLASS,CDC_COMMUNICATIONS_SUBCLASS_ACM,CDC_COMMUNICATIONS_PROTOCOL_V250,
    0x00,

    0x09,USB_DESCR_TYP_INTERF,
    0x01, /* interface number */
    0x00, /* alternate setting */
    0x01, /* number of endpoints */
    CDC_COMMUNICATIONS_INTERFACE_CLASS, CDC_COMMUNICATIONS_SUBCLASS_ACM, CDC_COMMUNICATIONS_PROTOCOL_V250, /* cdc device class */
    USB_STRING_DSC_PRODUCT, /* interface string index */


    0x05,USB_DESCR_TYP_CS_INTF,0x00,0x10,0x01,
    0x05,0x24,0x01,0x00,0x00,
    0x04,0x24,0x02,0x02,
    /* CDC Union */
    0x05,0x24,0x06,0x01,0x02,

    0x07,USB_DESCR_TYP_ENDP,
    0x81,               /* endpoint address 0x1, direction in */
    0x03,               /* interrupt, no sync, data endppint */
    USB_DESCR_WORD(8),  /* max packet size */
    0xFF,               /* poll interval */


    0x09,USB_DESCR_TYP_INTERF,
    0x02, /* interface number */
    0x00, /* alternate setting */
    0x02, /* number of endpoints */
    CDC_DATA_INTERFACE_CLASS, 0x00, 0x00,
    USB_STRING_DSC_PRODUCT, /* interface string index */

    0x07,USB_DESCR_TYP_ENDP,
    0x03, /* endpoint address 0x3, direction out */
    0x02, /* bulk, no sync, data endpoint */
    USB_DESCR_WORD(64),   /* max packet size */
    0x00, /* polling interval */

    0x07,USB_DESCR_TYP_ENDP,
    0x83, /* endpoint address 0x3, direction in */
    0x02, /* bulk, no sync, data endpoint */
    USB_DESCR_WORD(64),   /* max packet size */
    0x00, /* polling interval */
};

/* supported languages */
const unsigned char  __code usb_stringdescr_lang[] = {
  0x04, USB_DESCR_TYP_STRING,
  USB_DESCR_WORD(0x0409), /* us english */
};

const unsigned char  __code usb_stringdescr_serial[]       = "\x0A\x03" "n\x00" "o\x00" "n\x00" "e\x00";
const unsigned char  __code usb_stringdescr_product[]      = "\x20\x03" "U\x00" "S\x00" "B\x00" " \x00" "H\x00" "I\x00" "D\x00" " \x00" "D\x00" "i\x00" "s\x00" "p\x00" "l\x00" "a\x00" "y\x00";
const unsigned char  __code usb_stringdescr_manufacturer[] = "\x14\x03" "B\x00" "a\x00" "s\x00" "t\x00" "e\x00" "l\x00" "m\x00" "a\x00" "p\x00";

#pragma save
#pragma nooverlay

int16_t
Usb_GetDescriptor_Device(__idata struct UsbControlTransaction* transaction)
{
  switch(transaction->value)
  {
  case (USB_DESCR_TYP_DEVICE << 8):
    transaction->transfer_state.const_buffer = DevDesc;
    return sizeof(DevDesc);
  case (USB_DESCR_TYP_CONFIG << 8):
    transaction->transfer_state.const_buffer = CfgDesc;
    return sizeof(CfgDesc);
  case (USB_DESCR_TYP_STRING << 8) | 0:
    transaction->transfer_state.const_buffer = usb_stringdescr_lang;
    return transaction->transfer_state.const_buffer[0];
  case (USB_DESCR_TYP_STRING << 8) | USB_STRING_DSC_MANUFACTURER:
    transaction->transfer_state.const_buffer = usb_stringdescr_manufacturer;
    return transaction->transfer_state.const_buffer[0];
  case (USB_DESCR_TYP_STRING << 8) | USB_STRING_DSC_PRODUCT:
    transaction->transfer_state.const_buffer = usb_stringdescr_product;
    return transaction->transfer_state.const_buffer[0];
  case (USB_DESCR_TYP_STRING << 8) | USB_STRING_DSC_SERIAL:
    transaction->transfer_state.const_buffer = usb_stringdescr_serial;
    return transaction->transfer_state.const_buffer[0];
  default:
    return -1;
  }
}

int16_t
Usb_GetDescriptor_Interface(__idata struct UsbControlTransaction* transaction)
{
  if(transaction->index == 0)
  { /* Interface 0 is HID */
    switch(transaction->value)
    {
    case (USB_DESCR_TYP_HID << 8):
      transaction->transfer_state.const_buffer = CfgDesc + 2*0x9;
      return 0x9;
    case (USB_DESCR_TYP_REPORT << 8):
      transaction->transfer_state.const_buffer = UsbDesc_Report;
      return sizeof(UsbDesc_Report);
    default:
      break;
    }
  }

  return -1;
}

int_fast16_t
Usb_GetDescriptor_Setup(__idata struct UsbControlTransaction* transaction)
{
  __xdata USB_SETUP_REQ* setup_req = (__xdata USB_SETUP_REQ*)transaction->transfer_state.setup_out_buffer;
  int16_t setup_req_len;
  int16_t descriptor_len;

  if (transaction->request_recip == USB_REQ_RECIP_INTERF)
    descriptor_len = Usb_GetDescriptor_Interface(transaction);
  else
    descriptor_len = Usb_GetDescriptor_Device(transaction);

  if(setup_req->wLengthH >= 0x80)
    setup_req_len = INT16_MAX;
  else
    setup_req_len = ((int16_t)setup_req->wLengthH<<8) | (setup_req->wLengthL);

  if (setup_req_len < descriptor_len)
    descriptor_len = setup_req_len;

  if (descriptor_len > 0 && descriptor_len <= 8)
    memcpy(setup_req, transaction->transfer_state.const_buffer, descriptor_len);

  return descriptor_len;
}

#pragma restore

