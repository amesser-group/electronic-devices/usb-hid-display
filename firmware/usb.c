/**
 *  Copyright (c) 2021-2022 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"

__idata static struct UsbControlTransaction Usb_ep0_transaction_;
__idata static uint8_t usb_device_addr;
__idata        uint8_t usb_config_no;

void
Usb_Init()
{
  USB_CTRL   = 0x00;
  USB_CTRL   = bUC_SYS_CTRL0 | bUC_INT_BUSY | bUC_DMA_EN;
  USB_DEV_AD = 0x00;

  UDEV_CTRL  = bUD_PD_DIS | bUD_PORT_EN;

  UEP0_DMA = (uint16_t) Usb_Ep0_Buffer;
  UEP1_DMA = (uint16_t) Usb_Ep1_Buffer;
  UEP2_DMA = (uint16_t) Usb_Ep2_Buffer;
  UEP3_DMA = (uint16_t) Usb_Ep3_Buffer;

  UEP2_3_MOD = bUEP3_RX_EN | bUEP3_TX_EN |
               bUEP2_RX_EN | bUEP2_TX_EN ;

  UEP4_1_MOD = bUEP1_TX_EN;

  UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
  UEP1_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK;
  UEP2_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK | UEP_R_RES_ACK;
  UEP3_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK | UEP_R_RES_ACK;

  UEP0_T_LEN = 0;
  UEP1_T_LEN = 0;
  UEP2_T_LEN = 0;
  UEP3_T_LEN = 0;

  /* configure usb interrupts */
  USB_INT_EN  = bUIE_SUSPEND | bUIE_TRANSFER | bUIE_BUS_RST;
  USB_INT_FG  = 0x1F;
  IE_USB      = 1;
}

#pragma save
#pragma nooverlay
static int16_t
transaction_get_next_data(struct UsbTransferState* t, __xdata void* buffer, int8_t len)
{
  if(t->len < 0)
    return t->len;

  if(t->len < len)
    len = t->len;

  memcpy(buffer, t->const_buffer, len);

  t->len           -= len;
  t->const_buffer  += len;

  return len;
}

/** get first data of control transfer
 *
 * if t has assigned length <= 8, assume data has been placed in ep0 buffer already
 */
static int16_t
transaction_get_first_data(struct UsbTransferState* t, __xdata void* buffer, int8_t len)
{
  if(t->len <= 8)
  {
    len = t->len;
    t->len = 0;

    return len;
  }
  else
  {
    return transaction_get_next_data(t, buffer, len);
  }
}

#define ENDPOINT_HALT 0

static int16_t
Usb_HandleControlSetup(__idata struct UsbControlTransaction *transaction)
{
  switch(transaction->request)
  {
  case USB_GET_DESCRIPTOR:
    return Usb_GetDescriptor_Setup(transaction);
  case USB_SET_ADDRESS:
    usb_device_addr = transaction->value & 0xFF;
    return 0;
  case USB_GET_CONFIGURATION:
    transaction->transfer_state.setup_out_buffer[0] = usb_config_no;
    return 1;
  case USB_SET_CONFIGURATION:
    usb_config_no = transaction->value & 0xFF;
    return 0;
  case USB_GET_INTERFACE:
    return 0;
  case USB_CLEAR_FEATURE:
    if ( (transaction->request_recip == USB_REQ_RECIP_ENDP ) &&
         (transaction->value == ENDPOINT_HALT ) )

    {
      switch( transaction->index )
      {
      case 0x01:
        UEP1_CTRL = UEP1_CTRL & ~ ( bUEP_R_TOG | MASK_UEP_R_RES ) | UEP_R_RES_ACK;
        return 0;
      case 0x81:
        UEP1_CTRL = UEP1_CTRL & ~ ( bUEP_T_TOG | MASK_UEP_T_RES ) | UEP_T_RES_NAK;
        return 0;
      case 0x82:
        UEP2_CTRL = UEP2_CTRL & ~ ( bUEP_T_TOG | MASK_UEP_T_RES ) | UEP_T_RES_NAK;
        return 0;
      case 0x02:
        UEP2_CTRL = UEP2_CTRL & ~ ( bUEP_R_TOG | MASK_UEP_R_RES ) | UEP_R_RES_ACK;
        return 0;
      case 0x03:
      case 0x83: 
        return UsbCdc_ClearFeatureEndpointHalt(transaction->index);
      default:
        return -1;
      }
    }
    return -1;
  case USB_SET_FEATURE:
    if ( (transaction->request_recip == USB_REQ_RECIP_ENDP ) &&
         (transaction->value == ENDPOINT_HALT ) )
    {
      switch( transaction->index )
      {
      case 0x81:
        UEP1_CTRL = UEP1_CTRL & (~bUEP_T_TOG) | UEP_T_RES_STALL;
        return 0;
      case 0x01:
        UEP1_CTRL = UEP1_CTRL & (~bUEP_R_TOG) | UEP_R_RES_STALL;
        return 0;
      case 0x82:
        UEP2_CTRL = UEP2_CTRL & (~bUEP_T_TOG) | UEP_T_RES_STALL;
        return 0;
      case 0x02:
        UEP2_CTRL = UEP2_CTRL & (~bUEP_R_TOG) | UEP_R_RES_STALL;
        return 0;
      case 0x83:
        UEP3_CTRL = UEP3_CTRL & (~bUEP_T_TOG) | UEP_T_RES_STALL;
        return 0;
      case 0x03:
        UEP3_CTRL = UEP3_CTRL & (~bUEP_R_TOG) | UEP_R_RES_STALL;
        return 0;
      default:
        return -1;
      }
    }
    return -1;
  case USB_GET_STATUS:
    transaction->transfer_state.setup_out_buffer[0] = 0;
    transaction->transfer_state.setup_out_buffer[1] = 0;
    return 2;
  default:
    return -1;
  }
}

static int8_t
Usb_ParseSetup(__idata struct UsbControlTransaction *transaction)
{
  const __xdata USB_SETUP_REQ* setup_req = (USB_SETUP_REQ*)Usb_Ep0_Buffer;

  transaction->transfer_state.setup_out_buffer = Usb_Ep0_Buffer;
  transaction->transfer_state.len = -1;

  if (USB_RX_LEN != (sizeof(USB_SETUP_REQ)))
    return -1;

  transaction->request_typ   =  setup_req->bRequestType & USB_REQ_TYP_MASK;
  transaction->request_recip =  setup_req->bRequestType & USB_REQ_RECIP_MASK;
  transaction->request       =  setup_req->bRequest;
  transaction->value         = (setup_req->wValueH << 8) | (setup_req->wValueL);
  transaction->index         = (setup_req->wIndexH << 8) | (setup_req->wIndexL);

  return 0;
}
#pragma restore


void DeviceInterrupt(void) __interrupt (INT_NO_USB)
{
    int8_t len;

    if(UIF_TRANSFER)
    {
        switch (USB_INT_ST & (MASK_UIS_TOKEN | MASK_UIS_ENDP))
        {
        /* cdc interrupt */
        case UIS_TOKEN_IN | 1:
          UEP1_T_LEN = 0;
          UEP1_CTRL = UEP1_CTRL & ~ MASK_UEP_T_RES | UEP_T_RES_NAK;
          break;
        /* hid input pipe */
        case UIS_TOKEN_IN | 2:
          /* stop further in-transmissions */
          UEP2_T_LEN  = 0;
          UEP2_CTRL  |= UEP_T_RES_NAK;
          break;
        case UIS_TOKEN_OUT | 2:
          if ( U_TOG_OK )
          {
            /* stop further out processing */
            //usb_ep_out_jtag_bytecount = USB_RX_LEN;
            UEP2_CTRL |= UEP_R_RES_NAK;
          }
          break;
        /* CDC data communications */
        case UIS_TOKEN_IN | 3:
          usb_cdc_in_transfer();
          break;
        case UIS_TOKEN_OUT | 3:
          if ( U_TOG_OK )
              usb_cdc_out_transfer();
          break;
        case UIS_TOKEN_SETUP | 0:
          if(Usb_ParseSetup(&Usb_ep0_transaction_) >= 0)
          {
            if (Usb_ep0_transaction_.request_typ == USB_REQ_TYP_STANDARD )
            {
              Usb_ep0_transaction_.transfer_state.len = Usb_HandleControlSetup(&Usb_ep0_transaction_);
            }
            else if(Usb_ep0_transaction_.request_typ == USB_REQ_TYP_CLASS)
            {
              if ( Usb_ep0_transaction_.request_recip == USB_REQ_RECIP_INTERF)
              {
                switch(Usb_ep0_transaction_.index)
                {
                case 0:
                  Usb_ep0_transaction_.transfer_state.len = UsbHid_HandleControlSetup(&Usb_ep0_transaction_);
                  break;
                case 1:
                case 2:
                  Usb_ep0_transaction_.transfer_state.len = UsbCdc_HandleControlSetup(&Usb_ep0_transaction_);
                  break;
                default:
                  Display_DebugXData("Setup Bad Intf", Usb_Ep0_Buffer, 8);
                  break;
                }
              }
            }
            else
            {
              Display_DebugXData("Setup Bad Typ", Usb_Ep0_Buffer, 8);
            }
            
          }
          else
          {
            Display_DebugXData("Setup Invalid", Usb_Ep0_Buffer, 8);
          }
          
          

          len = transaction_get_first_data(&Usb_ep0_transaction_.transfer_state, Usb_Ep0_Buffer, DEFAULT_ENDP0_SIZE);

          if(len < 0)
          {
            UEP0_CTRL = bUEP_R_TOG | bUEP_T_TOG | UEP_R_RES_STALL | UEP_T_RES_STALL;
          }
          else if(len <= DEFAULT_ENDP0_SIZE)
          {
            UEP0_T_LEN = len;
            UEP0_CTRL = bUEP_R_TOG | bUEP_T_TOG | UEP_R_RES_ACK | UEP_T_RES_ACK;
          }
          else
          {
            UEP0_T_LEN = 0;
            UEP0_CTRL = bUEP_R_TOG | bUEP_T_TOG | UEP_R_RES_ACK | UEP_T_RES_ACK;
          }
          break;
        case UIS_TOKEN_IN | 0:
          switch(Usb_ep0_transaction_.request)
          {
          case USB_GET_DESCRIPTOR:
            UEP0_T_LEN = transaction_get_next_data(&Usb_ep0_transaction_.transfer_state, Usb_Ep0_Buffer, DEFAULT_ENDP0_SIZE);
            UEP0_CTRL ^= bUEP_T_TOG;
            break;
          case USB_SET_ADDRESS:
            USB_DEV_AD = USB_DEV_AD & bUDA_GP_BIT | usb_device_addr;
            UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
            break;
          default:
            UEP0_T_LEN = 0;
            UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
            break;
          }
          break;
        case UIS_TOKEN_OUT | 0:
          Usb_ep0_transaction_.transfer_state.len              = USB_RX_LEN;
          Usb_ep0_transaction_.transfer_state.setup_out_buffer = Usb_Ep0_Buffer;

          if (Usb_ep0_transaction_.request_typ == USB_REQ_TYP_CLASS )
          {
            switch(Usb_ep0_transaction_.index)
            {
            case 0:
              Usb_ep0_transaction_.transfer_state.len = UsbHid_HandleControlOut(&Usb_ep0_transaction_);
              break;
            case 1:
            case 2:
              Usb_ep0_transaction_.transfer_state.len = UsbCdc_HandleControlOut(&Usb_ep0_transaction_);
              break;
            default:
              Usb_ep0_transaction_.transfer_state.len = -1;
              break;
            }
          }
          else
          {
            Usb_ep0_transaction_.transfer_state.len = -1;
          }

          if(Usb_ep0_transaction_.transfer_state.len >= 0)
          {
              UEP0_T_LEN = 0;
              UEP0_CTRL |= UEP_R_RES_ACK | UEP_T_RES_ACK;
          }
          else
          {
              UEP0_T_LEN = 0;
              UEP0_CTRL |= UEP_R_RES_ACK | UEP_T_RES_NAK;
          }
          break;
        default:
          break;
        }

        UIF_TRANSFER = 0;
    }

    if(UIF_BUS_RST)
    { /* Bus reset */
        UEP0_CTRL = UEP_R_RES_ACK | UEP_T_RES_NAK;
        UEP1_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK;
        UEP2_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK | UEP_R_RES_ACK;
        UEP3_CTRL = bUEP_AUTO_TOG | UEP_T_RES_NAK | UEP_R_RES_ACK;

        USB_DEV_AD = 0x00;
        UIF_SUSPEND   = 0;
        UIF_TRANSFER  = 0;
        UIF_BUS_RST   = 0;
        usb_config_no = 0;
    }

    if (UIF_SUSPEND)
        UIF_SUSPEND = 0;

}
