/*****************************************************************************************************
 *   Copyright (C) 2011 Martin Schmoelzer <martin.schmoelzer@student.tuwien.ac.at>: Original version *
 *   Copyright (C) 2021 Andreas Messer <andi@bastelmap.de>: Adaptions for bastelino                  *
 *                                                                                                   *
 *   This program is free software; you can redistribute it and/or modify                            *
 *   it under the terms of the GNU General Public License as published by                            *
 *   the Free Software Foundation; either version 2 of the License, or                               *
 *   (at your option) any later version.                                                             *
 *                                                                                                   *
 *   This program is distributed in the hope that it will be useful,                                 *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of                                  *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                   *
 *   GNU General Public License for more details.                                                    *
 *                                                                                                   *
 *   You should have received a copy of the GNU General Public License                               *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 *****************************************************************************************************/
#ifndef BASTELINEO_PROGRAMMER_IO_H_
#define BASTELINEO_PROGRAMMER_IO_H_
#include "ch554.h"


/* PORT A */
#define PIN_U_OE      OUTA0
/* PA1 Not Connected */
#define PIN_OE        OUTA2
/* PA3 Not Connected */
#define PIN_RUN_LED   OUTA4
#define PIN_TDO       PINA5
#define PIN_BRKOUT    PINA6
#define PIN_COM_LED   OUTA7

/* PORT B */
#define PIN_TDI       OUTB0
#define PIN_TMS       OUTB1
#define PIN_TCK       OUTB2
#define PIN_TRST      OUTB3
#define PIN_BRKIN     OUTB4
#define PIN_RESET     OUTB5
#define PIN_OCDSE     OUTB6
#define PIN_TRAP      PINB7

/* JTAG Signals with direction 'OUT' on port B */
#define MASK_PORTB_DIRECTION_OUT (PIN_TDI | PIN_TMS | PIN_TCK | PIN_TRST | PIN_BRKIN | PIN_RESET | PIN_OCDSE)

/* PORT C */
#define PIN_RXD0      PINC0
#define PIN_TXD0      OUTC1
#define PIN_RESET_2   PINC2
/* PC3 Not Connecte */
/* PC4 Not Connected */
#define PIN_RTCK      PINC5
#define PIN_WR        OUTC6
/* PC7 Not Connected */

/* LED Macros */
#define SET_RUN_LED()     (OUTA &= ~PIN_RUN_LED)
#define CLEAR_RUN_LED()   (OUTA |=  PIN_RUN_LED)

#define SET_COM_LED()     (OUTA &= ~PIN_COM_LED)
#define CLEAR_COM_LED()   (OUTA |=  PIN_COM_LED)


/* Port 3.3 is connected through inverter with en */
SBIT(nESP32_EN,  0xB0, 3);
/* Port 1.1 is connected to IO0  */
SBIT(ESP32_IO0, 0x90, 1);

/* JTAG Pin definitions */

/* Port 1.4 is connected to TMS */
SBIT(ESP32_TMS, 0x90, 4);	
/* Port 1.5 is connected to TDI */
SBIT(ESP32_TDI, 0x90, 5);	
/* Port 1.6 is connected to TDO */
SBIT(ESP32_TDO, 0x90, 6);	
/* Port 1.7 is connected to TCK */
SBIT(ESP32_TCK, 0x90, 7);	


/* TRST and RESET are low-active and inverted by hardware. SET_HIGH de-asserts
 * the signal (enabling reset), SET_LOW asserts the signal (disabling reset) */
#define SET_TRST_HIGH()   (OUTB |=  PIN_TRST)
#define SET_TRST_LOW()    (OUTB &= ~PIN_TRST)

#define SET_RESET_HIGH()  (OUTB |=  PIN_RESET)
#define SET_RESET_LOW()   (OUTB &= ~PIN_RESET)

#define SET_OCDSE_HIGH()  (OUTB |=  PIN_OCDSE)
#define SET_OCDSE_LOW()   (OUTB &= ~PIN_OCDSE)

#define SET_BRKIN_HIGH()  (OUTB |=  PIN_BRKIN)
#define SET_BRKIN_LOW()   (OUTB &= ~PIN_BRKIN)

SBIT(LCD_E,   0xB0, 0); /**< Port 3.0 is connected to LCD E */
SBIT(LCD_RW,  0xB0, 2); /**< Port 3.2 is connected to LCD RW */
SBIT(LCD_RS,  0xB0, 3); /**< Port 3.3 is connected to LCD RS */

SBIT(LCD_PWR, 0xB0, 4); /**< Port 3.4 is connected to LCD Power Switch */


SBIT(IR_SIG, 0x90, 1); /**< Port 1.1 is connected to IR receiver */

#define SET_LCD_DB47(x) do { P1 = ((x) << 4) | 0x0F; } while(0);
#define GET_LCD_DB47()  (P1 >> 4)


#endif
