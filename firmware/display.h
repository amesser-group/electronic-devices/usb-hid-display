/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <stdint.h>

#define DISPLAY_ROWS    (2)
#define DISPLAY_COLUMNS (16)

#define DISPLAY_CHAR_HEIGHT (7)
#define DISPLAY_CHAR_WIDTH  (5)

extern __xdata uint8_t Display_buffer_[DISPLAY_COLUMNS * DISPLAY_ROWS];
extern __bit           Display_dirty_;

void Display_Init();
void Display_Refresh();

void Display_DebugXData(__code const char* msg, __xdata const uint8_t* hexdump, uint8_t len) __reentrant;
void Display_ShowMsgIntXData(__code const char* msg, uint8_t value, __xdata const uint8_t* data, uint8_t len) __reentrant;

#endif