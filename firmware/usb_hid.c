/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "usb_descriptor_hid.h"

static __idata uint8_t UsbHid_display_pos_ = 0;

#pragma save
#pragma nooverlay

#define USB_HID_REPORT_TYPE_FEATURE 0x03

__code static const uint8_t UsbHid_display_attributes_report_[] =
{
  USB_HID_REPORT_ID_DISPLAY_ATTRIBUTES_REPORT,
  ((DISPLAY_ROWS & 0x3F) << 2)    | ((DISPLAY_COLUMNS & 0x30) >> 4),
  ((DISPLAY_COLUMNS & 0x0F) << 4) | ((DISPLAY_CHAR_HEIGHT & 0x3C) >> 2),
  ((DISPLAY_CHAR_HEIGHT & 0x03) << 6) | ((DISPLAY_CHAR_WIDTH & 0x3F) >> 0) 
};

static int_fast16_t
UsbHid_GetReport_Setup(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->value)
  {
  case (USB_HID_REPORT_TYPE_FEATURE << 8) | USB_HID_REPORT_ID_DISPLAY_ATTRIBUTES_REPORT:
    memcpy(transaction->transfer_state.setup_out_buffer, UsbHid_display_attributes_report_,
           sizeof(UsbHid_display_attributes_report_));
    return sizeof(UsbHid_display_attributes_report_);
  default:
    Display_DebugXData("SetupGet BadFeat", Usb_Ep0_Buffer, 8);
    return -1;
  }
}

static int_fast16_t
UsbHid_SetReport_Setup(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->value)
  {
  case (USB_HID_REPORT_TYPE_FEATURE << 8) | USB_HID_REPORT_ID_CHARACTER_REPORT:
  case (USB_HID_REPORT_TYPE_FEATURE << 8) | USB_HID_REPORT_ID_CURSOR_POSITION_REPORT:
    return 0;
  default:
    Display_DebugXData("SetupH Bad Feat", Usb_Ep0_Buffer, 8);
    return -1;
  }
}

static void
UsbHid_SetReport_CharacterReport(__xdata const uint8_t* report)
{
  uint8_t i;

  for(i = 0; i < 4; ++i)
  {
    if (UsbHid_display_pos_ >= sizeof(Display_buffer_))
      break;

    Display_buffer_[UsbHid_display_pos_++] = report[1+i];
    Display_dirty_ = 1;
  }
}

static void
UsbHid_SetReport_CursorPositionReport(__xdata const uint8_t* report)
{
  UsbHid_display_pos_ = report[1] + report[2] * DISPLAY_COLUMNS;
}


static int_fast16_t
UsbHid_SetReport_Out(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->value )
  {
  case (USB_HID_REPORT_TYPE_FEATURE << 8) | USB_HID_REPORT_ID_CHARACTER_REPORT:
    UsbHid_SetReport_CharacterReport(transaction->transfer_state.setup_out_buffer);
    return 0;
  case (USB_HID_REPORT_TYPE_FEATURE << 8) | USB_HID_REPORT_ID_CURSOR_POSITION_REPORT:
    UsbHid_SetReport_CursorPositionReport(transaction->transfer_state.setup_out_buffer);
    return 0;
  default:
    return -1;
  }
}

int_fast16_t
UsbHid_HandleControlSetup(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->request )
  {
  case HID_GET_REPORT:  return UsbHid_GetReport_Setup(transaction);
  case HID_SET_REPORT:  return UsbHid_SetReport_Setup(transaction);
  default:              
    Display_DebugXData("SetupH Bad Req", Usb_Ep0_Buffer, 8);
    return -1;
  }
}

int_fast16_t
UsbHid_HandleControlOut(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->request )
  {
  case HID_SET_REPORT:  return UsbHid_SetReport_Out(transaction);
  default:              return -1;
  }
}

#pragma restore