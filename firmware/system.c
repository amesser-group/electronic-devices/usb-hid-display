/**
 *  Copyright (c) 2021-2024 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include "common.h"

__idata static uint8_t Clock_cksel_uart       = 0;
__idata uint16_t       System_timer_reload_1ms_div12 = 0;

__idata volatile uint8_t System_ticks;

__code const uint16_t Clock_reload_table_1ms_div12[8] =
{
  (uint16_t)(0 -   187500 / 12 / 1000),
  (uint16_t)(0 -   750000 / 12 / 1000),
  (uint16_t)(0 -  3000000 / 12 / 1000),
  (uint16_t)(0 -  6000000 / 12 / 1000),
  (uint16_t)(0 - 12000000 / 12 / 1000),
  (uint16_t)(0 - 16000000 / 12 / 1000),
  (uint16_t)(0 - 24000000 / 12 / 1000),
  (uint16_t)(0 - 30000000 / 12 / 1000)
};

#pragma save
#pragma nooverlay

void	
System_UpdateClock()
{
  uint8_t clock_cfg = Clock_cksel_uart;

  if(clock_cfg == 0)
  {
#if 0    
    if(JTAG_active)
      clock_cfg = SYS_CK_SEL_24MHZ;
    else
#endif
      clock_cfg = SYS_CK_SEL_12MHZ; /* slowest speed for working usb is 6mhz */
  }
  
  SAFE_MOD  = 0x55;
  SAFE_MOD  = 0xAA;
  CLOCK_CFG = (CLOCK_CFG & ~MASK_SYS_CK_SEL) | clock_cfg;
  SAFE_MOD  = 0x00;

  System_timer_reload_1ms_div12 = Clock_reload_table_1ms_div12[clock_cfg];
}


#pragma restore

void 
Timer0Isr(void) __interrupt (INT_NO_TMR0)
{
  TH0 = System_timer_reload_1ms_div12 / 256;
  TL0 = System_timer_reload_1ms_div12 % 256;

  ++System_ticks;

#if !IRTOY_MODE
  UsbCdc_SysTickIsr();
#endif
}

void
System_DelayMillis(uint8_t millis)
{
  uint8_t start = System_ticks;

  while((uint8_t)(System_ticks - start) < (uint8_t)(millis));
}

/* Don't access System ticks directly it might optimized by compiler */
uint8_t
System_GetSysTicks()
{
  return System_ticks;
}