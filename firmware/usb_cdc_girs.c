/**
 *  Copyright (c) 2021-2022 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>

#include "common.h"
#include "io.h"

#define USBCDC_RXFIFO_SIZE (64)

enum
{
  USB_CDC_SET_LINE_CODING        = 0x20,           // Configures DTE rate, stop-bits, parity, and number-of-character
  USB_CDC_GET_LINE_CODING        = 0x21,           // This request allows the host to find out the currently configured line coding.
  USB_CDC_SET_CONTROL_LINE_STATE = 0x22,           // This request generates RS-232/V.24 style control signals.
};

/** Timeout for GIRS Commands */
#define GIRS_IDLE_TIMEOUT   10000

/** Delay processing of receive command
 * 
 * This delay is needed to work around strange implementation in lirc daemon:
 * After sending the "receive" command, girs driver in lirc will drop any data
 * received within 1ms. This could lead to strange behavior */
#define GIRS_RECEIVE_DELAY 10


__idata static volatile uint16_t UsbCdc_idle_cnt_;
__bit   static          UsbCdc_idle_;
__bit   static          UsbCdc_dtr_;
__bit   static          UsbCdc_reset_request_;

__idata  static uint8_t UsbCdc_ir_idle_cnt_;

__xdata uint8_t UsbCdc_linecoding[7];

__idata static uint8_t UsbCdc_rxlen_    = 0;
__idata static uint8_t UsbCdc_rxoffset_ = 0;

__xdata static uint8_t  Girs_rxbuf_[USBCDC_RXFIFO_SIZE+1];
__idata static uint8_t  Girs_rxbuf_fill_ = 0;

#define GIRS_IRFIFO_DEPTH ((uint8_t)64)

__xdata static uint16_t UsbCdc_ir_fifo_[GIRS_IRFIFO_DEPTH];
__idata static uint16_t UsbCdc_ir_last_ts_;
__idata static uint8_t  UsbCdc_ir_fifo_in_  = 0;
__idata static uint8_t  UsbCdc_ir_fifo_out_ = 0;

#pragma save
#pragma nooverlay

void Timer2Isr(void) __interrupt (INT_NO_TMR2)
{
  if(EXF2)
  {
    uint16_t ts = RCAP2;

    if( UsbCdc_ir_idle_cnt_ > 0)
    {
      uint16_t delta = ts - UsbCdc_ir_last_ts_ ;

      if(((uint8_t)(UsbCdc_ir_fifo_in_ - UsbCdc_ir_fifo_out_)) < GIRS_IRFIFO_DEPTH)
      {
        if(delta >= 0x8000)
          delta = 0x7FFF;

        if(IR_SIG)
          delta |= 0x8000;

        UsbCdc_ir_fifo_[UsbCdc_ir_fifo_in_ % GIRS_IRFIFO_DEPTH] = delta;
        UsbCdc_ir_fifo_in_++;
      }
    }

    UsbCdc_ir_last_ts_  = ts;
    UsbCdc_ir_idle_cnt_ = 30;
    EXF2 = 0;
  }

  TF2 = 0;
}

void
usb_cdc_out_transfer()
{
  UsbCdc_rxlen_     = USB_RX_LEN;
  UsbCdc_rxoffset_  = 0;

  UEP3_CTRL |= UEP_R_RES_NAK;

  UsbCdc_idle_cnt_  = GIRS_IDLE_TIMEOUT;
  UsbCdc_idle_      = 0;
}

void
usb_cdc_in_transfer()
{
  /* clear all bits below 64. This is part of the statemachine to control short packet sending */
  UEP3_T_LEN = UEP3_T_LEN & 0xC0;
  UEP3_CTRL  |= UEP_T_RES_NAK;
}

void
UsbCdc_SysTickIsr()
{
  uint16_t idle_cnt = UsbCdc_idle_cnt_;

  if(idle_cnt > 1)
  {
    UsbCdc_idle_cnt_ = idle_cnt - 1;
  }
  else
  {
    UsbCdc_idle_cnt_ = 0;
    UsbCdc_idle_     = 1;
  }

  if(UsbCdc_ir_idle_cnt_ > 0)
    UsbCdc_ir_idle_cnt_--;
}


static
void UsbCdc_SetLineCoding(__xdata const uint8_t *linecoding)
{
  memcpy(UsbCdc_linecoding, linecoding, sizeof(UsbCdc_linecoding));
}

static
void UsbCdc_SetControlLineState(const __xdata USB_SETUP_REQ* setup_req)
{
  __bit dtr = (setup_req->wValueL & 0x01) != 0;

  if ( !UsbCdc_dtr_ && dtr)
  {
    UEP3_CTRL |= UEP_R_RES_NAK | UEP_T_RES_NAK;

    UEP3_T_LEN    = 0;
    UsbCdc_rxlen_ = 0;

    UsbCdc_reset_request_ = 1;
  }

  UsbCdc_dtr_ = dtr;

  #if 0
  bool dtr_asserted = setup_req->wValueL & 0x01;
  bool rts_asserted = setup_req->wValueL & 0x02;
#endif
}

int_fast16_t
UsbCdc_HandleControlSetup(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->request )
  {
  case USB_CDC_GET_LINE_CODING:
      memcpy(transaction->transfer_state.setup_out_buffer, UsbCdc_linecoding, sizeof(UsbCdc_linecoding));
      return sizeof(UsbCdc_linecoding);
  case USB_CDC_SET_LINE_CODING:
      return 0;
      break;
  case USB_CDC_SET_CONTROL_LINE_STATE:
      UsbCdc_SetControlLineState((const __xdata USB_SETUP_REQ*)(transaction->transfer_state.setup_out_buffer));
      return 0;
      break;
  default:
      return -1;
      break;
  }
}

int_fast16_t
UsbCdc_HandleControlOut(__idata struct UsbControlTransaction *transaction)
{
  switch( transaction->request )
  {
  case USB_CDC_SET_LINE_CODING:
    if(transaction->transfer_state.len != 7)
      return -1;

    UsbCdc_SetLineCoding(transaction->transfer_state.setup_out_buffer);
    return 0;
  default:
    return -1;
  }
}

int_fast16_t
UsbCdc_ClearFeatureEndpointHalt(uint8_t ep)
{
  /* To have this a Non-OP when EP not halted we will
   * clear the TOUT bit instead of assigning the entire bitmask:
   * Transitions STALL -> NAK, NAK -> NAK, ACK -> ACK */

  if(ep & 0x80)
  { /* IN (device->host, transmission*/
    UEP3_CTRL = UEP3_CTRL & (~bUEP_T_TOG) & (~UEP_T_RES_TOUT);
  }
  else
  { /* OUT (host->device, reception */
    UEP3_CTRL = UEP3_CTRL & (~bUEP_R_TOG) & (~UEP_R_RES_TOUT);
    UsbCdc_rxlen_ = 0;
  }

  return 0;
}

#pragma restore

static void
UsbCdc_RefillRxBuf()
{
  int8_t rxlen = UsbCdc_rxlen_;

  if(UsbCdc_rxoffset_ < rxlen)
  {
    uint8_t len_copy = rxlen - UsbCdc_rxoffset_;

    if( (Girs_rxbuf_fill_ + len_copy) > USBCDC_RXFIFO_SIZE)
      len_copy = USBCDC_RXFIFO_SIZE - Girs_rxbuf_fill_;

    if(len_copy > 0)
    {
      memcpy(Girs_rxbuf_ + Girs_rxbuf_fill_, 
            Usb_CDC_OutBuffer + UsbCdc_rxoffset_, 
            len_copy);

      Girs_rxbuf_fill_ += len_copy;
      UsbCdc_rxoffset_   += len_copy;

      Girs_rxbuf_[Girs_rxbuf_fill_] = 0;
    }
  }
  else
  { /* re-enable reception if all bytes taken from fifo */
    EA = 0; 
    if((UEP3_CTRL & MASK_UEP_R_RES) == UEP_R_RES_NAK )
    {
      UEP3_CTRL &= ~UEP_R_RES_NAK; 
    }
    EA = 1;
  }
}

static void
IRToy_DropRxBytes(uint8_t cnt)
{
  if (cnt < Girs_rxbuf_fill_)
  {
    memmove(Girs_rxbuf_, Girs_rxbuf_ + cnt, Girs_rxbuf_fill_ - cnt);
    Girs_rxbuf_fill_ -= cnt;
  }
  else
  {
    Girs_rxbuf_fill_ = 0;
  }
}

#define GIRS_RECV_EOL "\r"
#define GIRS_SEND_EOL "\n"

__code static const char Girs_String_OK[]      = "OK" GIRS_SEND_EOL;
__code static const char Girs_String_Version[] = "Bastelmap USB HID Display" GIRS_SEND_EOL;
__code static const char Girs_String_Modules[] = "Receive" GIRS_SEND_EOL;

__code static const char Girs_String_Timeout[] = "." GIRS_SEND_EOL;
__code static const char Girs_String_Trailer[] = "-32768" GIRS_SEND_EOL;

void
UsbCdc_TransmittBuffer()
{
  EA = 0; 
  if((UEP3_CTRL & MASK_UEP_T_RES) == UEP_T_RES_NAK )
  {
    UEP3_CTRL &= ~UEP_T_RES_NAK; 
  }
  else
  {
    UEP3_T_LEN  = 0;
  }
  EA = 1;
}

void UsbCdc_SendGirsResponse(__code const char* rsp)
{
  uint8_t offset = (UEP3_T_LEN % 64);

  //Display_DebugXData("GIRS cmd ok", Girs_rxbuf_, 8);

  while(offset < sizeof(Usb_CDC_InBuffer))
  {
    uint8_t c = *(rsp++);

    Usb_CDC_InBuffer[offset++] = c;
    
    if(c == '\n')
      break;
  }

  UEP3_T_LEN = offset;
  UsbCdc_TransmittBuffer();
}

__bit static UsbCdc_ir_data_sent_ = 0;

void UsbCdc_SendGirsResponse_Init(__code const char* rsp)
{
  UsbCdc_SendGirsResponse(rsp);
  UsbCdc_ir_data_sent_ = 1;
}

uint8_t
UsbCdc_AppendGirsTiming(uint8_t offset)
{
  uint16_t entry;

  entry = UsbCdc_ir_fifo_[UsbCdc_ir_fifo_out_ % GIRS_IRFIFO_DEPTH];
  UsbCdc_ir_fifo_out_++;

  if(entry & 0x8000)
    Usb_CDC_InBuffer[offset++] = '+';
  else
    Usb_CDC_InBuffer[offset++] = '-';

  __uitoa(entry & 0x7FFF, Usb_CDC_InBuffer + offset, 10);

  while(offset < sizeof(Usb_CDC_InBuffer))
  {
    if(Usb_CDC_InBuffer[offset] == '\0')
      break;
    ++offset;
  }

  return offset;
}

static __xdata uint8_t*
UsbCdc_HandleGirsCommand_Receive(__xdata uint8_t* p)
{
  uint16_t idle_cnt;

  do {idle_cnt = UsbCdc_idle_cnt_;} while(idle_cnt != UsbCdc_idle_cnt_);

  if(idle_cnt > (GIRS_IDLE_TIMEOUT -  GIRS_RECEIVE_DELAY))
  {
    return NULL;
  }
  else if(UsbCdc_ir_fifo_in_ != UsbCdc_ir_fifo_out_)
  {
    uint8_t offset = (UEP3_T_LEN % 64);

    if(offset < (sizeof(Usb_CDC_InBuffer) - 7))
      offset = UsbCdc_AppendGirsTiming(offset);
    
    UEP3_T_LEN = offset;
    if(offset >= (sizeof(Usb_CDC_InBuffer) - sizeof(Girs_String_Trailer)))
      UsbCdc_TransmittBuffer();

    UsbCdc_ir_data_sent_ = 1;
    return NULL;
  }
  else if(UsbCdc_ir_idle_cnt_ != 0)
  {
    return NULL;
  }
  else if(UsbCdc_ir_data_sent_)
  {
    UsbCdc_ir_data_sent_ = 0;
    UsbCdc_SendGirsResponse(Girs_String_Trailer);
    return p;
  }
  else if (UsbCdc_idle_)
  {
    UsbCdc_SendGirsResponse(Girs_String_Timeout);
    return p;
  }
  else
  {
    if(UEP3_T_LEN >= 64)
    { /* send a short packet to terminate last transmission */
      UEP3_T_LEN = 0;
      UsbCdc_TransmittBuffer();
    }

    return NULL;
  }
}

static void
UsbCdc_GirsFinalizeCommand(__xdata uint8_t *p)
{
  if(p != NULL)
    UsbCdc_GirsTrimBuffer(p+1);
}

void
UsbCdc_Init()
{
  UsbCdc_rxlen_    = 0;
  UsbCdc_rxoffset_ = 0;

  Girs_rxbuf_fill_ = 0;

  EXEN2  = 1;
  CP_RL2 = 1;
  TR2    = 1;
}

void
UsbCdc_ResetGirs()
{
  UsbCdc_ir_fifo_out_ = UsbCdc_ir_fifo_in_;
  UsbCdc_ir_idle_cnt_ = 0;

  if(Girs_rxbuf_fill_ > 0)
  {
    Display_ShowMsgIntXData("GIRS timout", Girs_rxbuf_fill_, Girs_rxbuf_, 8);
    UsbCdc_GirsTrimBuffer(Girs_rxbuf_ + USBCDC_RXFIFO_SIZE);
  }
}

void
UsbCdc_Poll()
{
  if(UsbCdc_reset_request_)
  {
    UsbCdc_reset_request_ = 0;
    UsbCdc_ResetGirs();

    /* lircd's girs driver expects a line from device after playing with dtr.
     * Otherwise it will trap in synchronize sequence which fails quite often
     * (It reads the response "OK" of previous command as version) */
    UsbCdc_SendGirsResponse(Girs_String_OK);
  }
  else
  {
    __xdata uint8_t* p;

    UsbCdc_RefillRxBuf();

    if((p = memchr(Girs_rxbuf_, '\r', Girs_rxbuf_fill_)) != NULL)
    {
      if((UEP3_CTRL & MASK_UEP_T_RES) == UEP_T_RES_NAK)
      {
        if(strncmp(Girs_rxbuf_, "receive\r", 8) == 0)
          p = UsbCdc_HandleGirsCommand_Receive(p);
        else if(strncmp(Girs_rxbuf_, "\r", 1) == 0)
          UsbCdc_SendGirsResponse(Girs_String_OK);
        else if(strncmp(Girs_rxbuf_, "version\r", 8) == 0)
          UsbCdc_SendGirsResponse_Init(Girs_String_Version);
        else if(strncmp(Girs_rxbuf_, "modules\r", 8) == 0)
          UsbCdc_SendGirsResponse_Init(Girs_String_Modules);
        else
          Display_ShowMsgIntXData("GIRS bad cmd", Girs_rxbuf_fill_, Girs_rxbuf_, 8);

        UsbCdc_GirsFinalizeCommand(p);
      }
    }
    else if (Girs_rxbuf_fill_ >= USBCDC_RXFIFO_SIZE)
    { /* empty entire buffer, no command inside */
      Display_DebugXData("GIRS ovf", Girs_rxbuf_, 8);
      UsbCdc_GirsTrimBuffer(Girs_rxbuf_ + USBCDC_RXFIFO_SIZE);
    }
    else if ( UsbCdc_idle_)
    {
      UsbCdc_ResetGirs();
    }
  }
}
