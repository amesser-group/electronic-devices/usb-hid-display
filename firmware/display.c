/**
 *  Copyright (c) 2021 Andreas Messer <andi@bastelmap.de>
 * 
 *  This file is part of usb hid display firmware.
 *
 *  usb hid display firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  usb hid display firmware is distributed in the
 *  hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with usb hid display firmware.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "common.h"
#include "display.h"
#include "io.h"


__xdata uint8_t Display_buffer_[DISPLAY_COLUMNS * DISPLAY_ROWS];
__bit           Display_dirty_ = 0;

__idata uint8_t Display_ticks_   = 0;
__idata uint8_t Display_timeout_ = 0;

static __code const char Display_splash_[] = "USB HID Display No connection!  ";

static void
Display_WriteNibble(uint8_t data)
{
  LCD_RW = 0;
  P1     = data | 0x0F;
  LCD_E  = 1;
  LCD_E  = 0;
}

static void
Display_WriteByte(uint8_t data)
{
  Display_WriteNibble(data);
  Display_WriteNibble(data << 4);
}

static uint8_t
Display_ReadByte()
{
  uint8_t data;

  P1     = 0xFF;
  LCD_RW = 1;
  LCD_E  = 1;
  data   = (P1 & 0xF0);
  LCD_E  = 0;
  LCD_E  = 1;
  data  |= (P1 >> 4);
  LCD_E  = 0;

  return data;
}

static void
Display_WaitReady()
{
  uint8_t t;

  LCD_RS = 0;
  t = System_ticks;
  do {
    if ( (Display_ReadByte() & 0x80) == 0)
      break;
  } while( (uint8_t)(System_ticks - t) < 10);
}

static void
Display_WriteCommand(uint8_t cmd)
{
  LCD_RS = 0;
  Display_WriteByte(cmd);
  Display_WaitReady();
}

static void
Display_WriteData(uint8_t data)
{
  LCD_RS = 1;
  Display_WriteByte(data);
}

void
Display_Init()
{
  /* powerup lcd */
  LCD_E   = 0;
  LCD_RS  = 0;
  LCD_PWR = 0;

  System_DelayMillis(250);
  System_DelayMillis(250);

  Display_WriteNibble(0x20); /* 4bit mode */

  Display_WriteCommand(0x28); /* 4bit mode, 2 lines, 5x8, ft 0 */
  Display_WriteCommand(0x08); /* display, cursor, blinking off */
  Display_WriteCommand(0x01); /* display clear */
  Display_WriteCommand(0x02); /* home */
  Display_WriteCommand(0x06); /* increment position, no shift */
  Display_WriteCommand(0x0C); /* display on, cursor off, blinking off*/

  memcpy(Display_buffer_, Display_splash_, sizeof(Display_buffer_));
  Display_dirty_ = 1;
}

static void Display_WriteRow(__xdata const uint8_t* data)
{
  uint8_t i = 0;
  for(i  = 0; i < DISPLAY_COLUMNS; ++i)
    Display_WriteData(data[i]);
}

#pragma save
#pragma nooverlay

static uint8_t
Display_ShowRaw(__xdata const uint8_t* raw, uint8_t len, uint8_t offset) __reentrant
{
  while( (offset < sizeof(Display_buffer_)) && (len > 0))
  {
    Display_buffer_[offset++] = *(raw++);
    len--;
  }

  return offset;
}

static uint8_t
Display_ShowString(__code const char* s, uint8_t offset, uint8_t stop_offset) __reentrant
{
  while(offset < stop_offset)
  {
    if (*s != 0)
      Display_buffer_[offset++] = *(s++);
    else
      Display_buffer_[offset++] = ' ';
  }

  return offset;
}

static uint8_t
Display_DumpHex(uint8_t offset, __xdata const uint8_t* hexdump, uint8_t len) __reentrant
{
  __code static const uint8_t hex_charmap[] = "0123456789ABCDEF";

  while( ((offset + 1) < sizeof(Display_buffer_)) && (len > 0))
  {
    Display_buffer_[offset++] = hex_charmap[(*hexdump >> 4) & 0xF];
    Display_buffer_[offset++] = hex_charmap[(*hexdump >> 0) & 0xF];
    len--;
    hexdump++;
  }

  return offset;
}

static void
Display_StuffRemainder(uint8_t offset) __reentrant
{
  while(offset < sizeof(Display_buffer_))
    Display_buffer_[offset++] = ' ';

  Display_dirty_ = 1;
}


void
Display_DebugXData(__code const char* msg, __xdata const uint8_t* hexdump, uint8_t len) __reentrant
{
  uint8_t i;

  i = Display_ShowString(msg, 0, DISPLAY_COLUMNS);
  i = Display_DumpHex(i, hexdump, len);
  Display_StuffRemainder(i);
}

void
Display_ShowMsgIntXData(__code const char* msg, uint8_t value, __xdata const uint8_t* data, uint8_t len) __reentrant
{
  uint8_t i;

  i = Display_ShowString(msg, 0, DISPLAY_COLUMNS-2);

  Display_buffer_[i++] = '0' + value / 10;
  Display_buffer_[i++] = '0' + value % 10;

  i = Display_ShowRaw(data, len, i);
  Display_StuffRemainder(i);
}

#pragma restore

void
Display_Refresh()
{
  uint8_t ticks = System_GetSysTicks();
  
  if (Display_dirty_)
  {
    Display_ticks_   = ticks;
    Display_timeout_ = 200;
  }
  else if(Display_timeout_ > 0)
  {

    if((uint8_t)(Display_ticks_ - ticks) > 100)
    {
      Display_ticks_ += 100;
      Display_timeout_--;
    }

    if(Display_timeout_ == 0)
    {
      Display_StuffRemainder(0);
      Display_dirty_ = 1;
    }
  }

  if (Display_dirty_)
  {
    Display_dirty_ = 0;

    Display_WriteCommand(0x80U);
    Display_WriteRow(&(Display_buffer_[DISPLAY_COLUMNS * 0]));

    Display_WriteCommand(0x80U + 0x40U);
    Display_WriteRow(&(Display_buffer_[DISPLAY_COLUMNS * 1]));
  }
}
